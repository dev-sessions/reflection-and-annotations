package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
@UserRole(Role.USER)
open class Guest(val name: String, var email: String)