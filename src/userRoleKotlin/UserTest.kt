package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
fun main(args: Array<String>) {
    var user = User("Vasya", "Pupkin", 20, "vasya@gmail.com")
    login(user)

    var admin = Admin("Julia", "Skvortsova", 27, "email@gmail.com")
    login(admin)

    var superUser = SuperUser("John", "Doe", 35, "john@gmail.com")
    login(superUser)

    var guest = Guest("princess", "pokemon@gmail.com")
    login(guest)
}

fun<T : Guest> login(user: T){
    val userClass = user::class
    val userRoleAnnotation = userClass.annotations.first { it is UserRole } as? UserRole
    val role = userRoleAnnotation?.role ?: throw Exception("No permissions")
    println("kotlin.User role $role, name ${user.name}, permissions: ")
    showPermissions(role?.getPermissions())
}

fun showPermissions(permissions: List<Permission>){
    for (permission in permissions) {
        println(permission)
    }
}