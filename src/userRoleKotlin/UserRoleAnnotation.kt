package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class UserRole(val role: Role)