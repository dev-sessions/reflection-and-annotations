package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
@UserRole(Role.USER)
open class User(name: String, val surname: String, var age: Int, email: String) : Guest(name, email)