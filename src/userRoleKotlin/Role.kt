package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
enum class Role
{
    ADMIN{
        override fun getPermissions(): List<Permission> {
            return listOf(Permission.EDIT, Permission.OWNER, Permission.READ, Permission.WRITE)
        }
    },
    USER{
        override fun getPermissions(): List<Permission> {
            return listOf(Permission.READ)
        }
    },
    SUPERUSER{
        override fun getPermissions(): List<Permission> {
            return listOf(Permission.READ, Permission.WRITE, Permission.EDIT)
        }
    };

    abstract fun getPermissions() : List<Permission>
}