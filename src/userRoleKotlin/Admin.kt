package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
@UserRole(Role.ADMIN)
class Admin(name: String, surname: String, age: Int, email: String) : User(name, surname, age, email)