package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
@UserRole(Role.SUPERUSER)
class SuperUser(name: String, surname: String, age: Int, email: String) : User(name, surname, age, email)