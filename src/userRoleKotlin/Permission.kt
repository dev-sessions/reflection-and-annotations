package userRoleKotlin

/**
 * Created by juliaskvortsova on 7/11/17.
 */
enum class Permission
{
    READ,
    WRITE,
    EDIT,
    OWNER
}