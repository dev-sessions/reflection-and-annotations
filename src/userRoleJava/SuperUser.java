package userRoleJava;

/**
 * Created by juliaskvortsova on 7/12/17.
 */
@UserRoleAnnotation(role = Role.SUPERUSER)
public class SuperUser extends User {

    public SuperUser(String name, String surname, int age, String email){
        super(name, surname, age, email);
    }
}
