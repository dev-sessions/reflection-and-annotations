package userRoleJava;

import java.util.Arrays;
import java.util.List;

/**
 * Created by juliaskvortsova on 7/11/17.
 */


public enum Role
{
    ADMIN{
        @Override
        List<Permission> getPermissions() {
            return Arrays.asList(
                    Permission.EDIT, Permission.OWNER, Permission.READ, Permission.WRITE);
        }
    },
    USER{
        @Override
        List<Permission> getPermissions() {
            return Arrays.asList(Permission.READ);
        }
    },
    SUPERUSER{
        @Override
        List<Permission> getPermissions(){
            return Arrays.asList(Permission.READ, Permission.WRITE, Permission.EDIT);
        }
    };

    abstract List<Permission> getPermissions();
}