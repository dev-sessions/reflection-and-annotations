package userRoleJava;


/**
 * Created by juliaskvortsova on 7/12/17.
 */
@UserRoleAnnotation(role = Role.ADMIN)
public class Admin extends User {

    public Admin(String name, String surname, int age, String email) {
        super(name, surname, age, email);
    }
}
