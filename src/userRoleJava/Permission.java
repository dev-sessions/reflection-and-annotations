package userRoleJava;

/**
 * Created by juliaskvortsova on 7/11/17.
 */
public enum Permission
{
    READ,
    WRITE,
    EDIT,
    OWNER
}