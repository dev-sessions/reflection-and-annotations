package userRoleJava;

/**
 * Created by juliaskvortsova on 7/12/17.
 */
@UserRoleAnnotation(role = Role.USER)
public class User extends Guest {
    private String surname;
    private int age;

    public User(String name, String surname, int age, String email) {
        super(name, email);
        this.surname = surname;
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
