package userRoleJava;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * Created by juliaskvortsova on 7/12/17.
 */
public class UserTest {
    public static void main(String[] args) {
        Guest guest = new Guest("princess", "pokemon@gmail.com");
        login(guest);

        User user = new User("Vasya", "Pupkin", 35, "vasya@gmail.com");
        login(user);
        User admin = new Admin("Julia", "Skvortsova", 27, "julia@gmail.com");
        login(admin);
        User superUser = new SuperUser("John", "Doe", 30, "john@gmail.com");
        login(superUser);

    }

    public static void login(Guest user){
        Class userClass = user.getClass();
        Annotation annotation = userClass.getAnnotation(UserRoleAnnotation.class);
        if(annotation != null){
            UserRoleAnnotation userRoleAnnotationAnnotation = (UserRoleAnnotation)annotation;
            Role role = userRoleAnnotationAnnotation.role();
            System.out.println(String.format("User role %s, name %s, permissions: ", role, user.getName()));
            showPermissions(role.getPermissions());
        }
    }

    private static void showPermissions(List<Permission> permissions){
        for (Permission permission : permissions) {
            System.out.println(permission);
        }
    }
}
